#!/bin/bash
mkdir -p /mnt/data

aws s3 cp s3://viascience-projects/cybersecurity-internal-threat/r6.2/email.csv /mnt/data/

hdfs dfs -put /mnt/data/ /user/hadoop/email

exit 0

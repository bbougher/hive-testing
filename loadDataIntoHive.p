CREATE TABLE emailtext (id STRING,
datetime DATE, employeeID STRING,
pc STRING, recipient STRING, cc STRING, bcc STRING,
sender STRING, activity STRING, size INT, attachments STRING,
content STRING)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE location '/user/hadoop/email';

CREATE TABLE email (id STRING,
datetime DATE, employeeID STRING,
pc STRING, recipient STRING, cc STRING, bcc STRING,
sender STRING, activity STRING, size INT, attachments STRING,
content STRING)
STORED AS ORC TBLPROPERTIES ('orc.compress'='SNAPPY',
'orc.create.index'='true',
'orc.bloom.filter.columns'='',
'orc.bloom.filter.fpp'='0.05',
'orc.stripe.size'='268435456',
'orc.row.index.stride'='10000');


INSERT OVERWRITE TABLE email SELECT * FROM emailtext SORT BY (employeeID, datetime);
